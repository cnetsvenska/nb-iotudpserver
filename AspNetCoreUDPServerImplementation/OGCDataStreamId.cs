﻿using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading;

namespace AspNetCoreUDPServerImplementation
{
    class OGCDataStreamId
    {
       
        string url = "http://ResourceCatalogue/SearchOrCreateOGCDataStreamId";

        string mqttInfo = "http://ResourceCatalogue/GetMQTTInfo";

        public JObject ReceiveId(string id, string metadata, string type, string unit, double lat, double lon)
        {
            JObject json =
            new JObject(
                new JProperty("externalId", id),
                new JProperty("metadata", metadata),
                new JProperty("sensorType", type),
                new JProperty("unitOfMeasurement", unit),
                new JProperty("fixedLatitude", lat),
                new JProperty("fixedLongitude", lon)
            );

            WebClient client = new WebClient();
            try
            {
                client.Encoding = Encoding.UTF8;
                client.Headers["Accept"] = "application/json";
                client.Headers["Content-Type"] = "application/json";
                string response = client.UploadString(url, json.ToString());
                var jsonresult = JObject.Parse(response);

                return jsonresult;

            }
            catch (WebException exception)
            {
                System.Console.WriteLine("Invokation error" + url + "HUBMessage" + exception.Message);
                return null;
            }
        }

        public string GetMqttServer()
        {

            WebClient client = new WebClient();
            try
            {
                string response = client.DownloadString(mqttInfo);
                var jsonresult = JObject.Parse(response);

                return jsonresult["mqttServer"].ToString();

            }
            catch (WebException exception)
            {
                System.Console.WriteLine("Invokation error" + url + "HUBMessage" + exception.Message);
                return null;
            }
        }
        public static string GetJson(int iotId, string timestamp, double latitude, double longitude, double value, string type)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("sv-SE");
            DateTime time = DateTime.ParseExact(timestamp, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);

            JObject json =
                new JObject(
                    new JProperty("resultTime", time.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    new JProperty("Datastream",
                        new JObject(
                            new JProperty("@iot.id", iotId))),
                     new JProperty("FeatureOfInterest",
                        new JObject(
                            new JProperty("@iot.id", "1"))),
                    new JProperty("phenomenonTime", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    new JProperty("result",
                        new JObject(
                            new JProperty("valueType", type),
                            new JProperty("Position",
                                new JObject(
                                    new JProperty("type", "Point"),
                                    new JProperty("coordinate", new JArray(latitude, longitude)))),
                            new JProperty("response",
                                new JObject(
                                    new JProperty("value", value))))));

            return json.ToString();
        }

        public static string GetJsonString(int iotId, string timestamp, double latitude, double longitude, string value, string type)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("sv-SE");
            DateTime time = DateTime.ParseExact(timestamp, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);

            JObject json =
                new JObject(
                    new JProperty("resultTime", time.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    new JProperty("Datastream",
                        new JObject(
                            new JProperty("@iot.id", iotId))),
                     new JProperty("FeatureOfInterest",
                        new JObject(
                            new JProperty("@iot.id", "1"))),
                    new JProperty("phenomenonTime", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    new JProperty("result",
                        new JObject(
                            new JProperty("valueType", type),
                            new JProperty("Position",
                                new JObject(
                                    new JProperty("type", "Point"),
                                    new JProperty("coordinate", new JArray(latitude, longitude)))),
                            new JProperty("response",
                                new JObject(
                                    new JProperty("value", value))))));

            return json.ToString();
        }
    }
}
