﻿using MQTTnet.ManagedClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AspNetCoreUDPServerImplementation
{
    public class SystemInfo
    {
        /// <summary>
        /// Pretty print an incoming system information update
        /// </summary>
        /// <param name="payload">The raw message</param>
        public static void PrintSystemInfo(string payload)
        {
            //starting,352753093224036,3670,5
            string[] parameters = payload.Split(',');
            if(parameters.Length == 2)
            {
                Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmss") + parameters[1] + " has started");
            }
            else if (parameters.Length == 4) {
                Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmss") + parameters[1] + " has started with battery level " + Convert.ToDouble(parameters[2]) / 1000.0 + "V, restart cause: " + parameters[3]);
            }
        }

        /// <summary>
        /// Parse the system info message and post it to the correct datastream
        /// </summary>
        /// <param name="msg">The raw message</param>
        /// <param name="ml">The MQTT client to use</param>
        public static void HandleSysInfo(string msg, ManagedMqttClient ml)
        {
            string[] parameters = msg.Split(',');

            if (parameters.Length != 4)
            {
                return;
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;     // needed for doubles to accept dots as decimal points
            string source = parameters[1];
            double batLevel = Convert.ToDouble(parameters[2]);
            /*
             Reset cause can be any of the following integers, mainly 0, 5 and 6
             0  Power On Reset
             1  Brown Out 12 Detector Reset
             2  Brown Out 33 Detector Reset
             3  Reserved
             4  External Reset
             5  Watchdog Reset
             6  System Reset Request
             7  Reserved
             */
            int resetCause = Convert.ToInt32(parameters[3]);
            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            // ask connection manager which topic and id to use when publishing 
            var ogcId = new OGCDataStreamId();
            JObject result = ogcId.ReceiveId(source, "NB-IoT mobile platform", "Startup", "Restart Cause", 0.0, 0.0);

            try
            {
                var dataStreamId = result["dataStreamId"];
                var mqttTopic = result["mqttTopic"];
                var mqttServer = result["mqttServer"];

                if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                {
                    Console.WriteLine("Did not receive a datastream ID from the remote service");
                    return;
                }

                int iotId = Convert.ToInt32(dataStreamId);

                string json = OGCDataStreamId.GetJsonString(iotId, timestamp, 0.0, 0.0, GetResetCause(resetCause), "Restart Cause");

                byte[] buffer = Encoding.UTF8.GetBytes(json);
                MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
            }
            catch (Exception e)
            {
                Console.WriteLine("HandleHeartbeat error: " + e.Message);
            }

            if (batLevel > 0.0)
            {
                result = ogcId.ReceiveId(source, "NB-IoT mobile platform", "Battery Status", "V", 0.0, 0.0);

                try
                {
                    var dataStreamId = result["dataStreamId"];
                    var mqttTopic = result["mqttTopic"];
                    var mqttServer = result["mqttServer"];

                    if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                    {
                        Console.WriteLine("Did not receive a datastream ID from the remote service");
                        return;
                    }

                    int iotId = Convert.ToInt32(dataStreamId);

                    string json = OGCDataStreamId.GetJson(iotId, timestamp, 0.0, 0.0, batLevel / 1000.0, "V");

                    byte[] buffer = Encoding.UTF8.GetBytes(json);
                    MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
                }
                catch (Exception e)
                {
                    Console.WriteLine("HandleSound error: " + e.Message);
                }
            }
        }

        private static string GetResetCause(int resetCode)
        {
            switch (resetCode)
            {
                case 0:
                    return "Power On Reset";
                case 1:
                    return "Brown Out 12 Detector Reset";
                case 2:
                    return "Brown Out 33 Detector Reset";
                case 3:
                    return "Reserved";
                case 4:
                    return "External Reset";
                case 5:
                    return "Watchdog Reset";
                case 6:
                    return "System Reset Request";
                default:
                    return "Reset reason unknown";
            }
        }
    }
}
