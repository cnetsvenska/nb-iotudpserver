﻿using MQTTnet.ManagedClient;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Text;
using System.Threading;

namespace AspNetCoreUDPServerImplementation
{
    public class SoundLevel
    {
        /// <summary>
        /// Pretty print the incoming sound measurement
        /// </summary>
        /// <param name="payload">The raw message sent from a box</param>
        public static void PrintSound(string payload)
        {
            //sound,35.4,44.6,39.3,37.7,59.3996285,18.0346580,84408.00,352753093224036
            string[] parameters = payload.Split(',');
            string time = Helper.ParseTimestamp(parameters[7]);
            // currently there are two versions of the box, one sending battery status and one which doesn't
            if (parameters.Length == 10)
            {
                Console.WriteLine(time + " >> SPL " + parameters[3] + "dBA at location: " + parameters[5] + "," + parameters[6] + " from " + parameters[8] + " with battery lvl: " + Convert.ToDouble(parameters[9]) / 1000.0 + "V");
            }
            else if (parameters.Length == 9)
            {
                Console.WriteLine(time + " >> SPL " + parameters[3] + "dBA at location: " + parameters[5] + "," + parameters[6] + " from " + parameters[8]);
            }
        }

        /// <summary>
        /// Parse the sound measurement message and post it to the correct datastream
        /// </summary>
        /// <param name="msg">Raw incoming sound measurement</param>
        /// <param name="ml">The MQTT client used to post the observation</param>
        public static void HandleSound(string msg, ManagedMqttClient ml)
        {
            string[] parameters = msg.Split(',');

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;     // needed for doubles to accept dots as decimal points
            string type = parameters[0];
            // TODO: use all three for one message or send them separately? 
            double minSound = Convert.ToDouble(parameters[1]);
            double maxSound = Convert.ToDouble(parameters[2]);
            double avgSound = Convert.ToDouble(parameters[3]);
            double medSound = Convert.ToDouble(parameters[4]);
            double latitude = Convert.ToDouble(parameters[5]);
            double longitude = Convert.ToDouble(parameters[6]);
            string timestamp = Helper.ParseTimestamp(parameters[7]);
            string sender = parameters[8];
            double batLevel = 0.0;
            // depending on box version
            if (parameters.Length == 10)
            {
                batLevel = Convert.ToDouble(parameters[9]);
            }

            // ask connection manager which topic and id to use when publishing 
            var ogcId = new OGCDataStreamId();

            JObject result = ogcId.ReceiveId(sender, "NB-IoT mobile platform", "SPL", "dBA", latitude, longitude);

            try
            {
                var dataStreamId = result["dataStreamId"];
                var mqttTopic = result["mqttTopic"];
                var mqttServer = result["mqttServer"];

                if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                {
                    Console.WriteLine("Did not receive a datastream ID from the remote service");
                    return;
                }

                int iotId = Convert.ToInt32(dataStreamId);

                string json = OGCDataStreamId.GetJson(iotId, timestamp, latitude, longitude, avgSound, "dBA");

                byte[] buffer = Encoding.UTF8.GetBytes(json);
                MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
            }
            catch (Exception e)
            {
                Console.WriteLine("HandleSound error: " + e.Message);
            }

            if (batLevel != 0.0)
            {
                result = ogcId.ReceiveId(sender, "NB-IoT mobile platform", "Battery Status", "V", latitude, longitude);

                try
                {
                    var dataStreamId = result["dataStreamId"];
                    var mqttTopic = result["mqttTopic"];
                    var mqttServer = result["mqttServer"];

                    if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                    {
                        Console.WriteLine("Did not receive a datastream ID from the remote service");
                        return;
                    }

                    int iotId = Convert.ToInt32(dataStreamId);

                    string json = OGCDataStreamId.GetJson(iotId, timestamp, latitude, longitude, batLevel/1000.0, "V");

                    byte[] buffer = Encoding.UTF8.GetBytes(json);
                    MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
                }
                catch (Exception e)
                {
                    Console.WriteLine("HandleSound error: " + e.Message);
                }
            }
        }
    }
}
