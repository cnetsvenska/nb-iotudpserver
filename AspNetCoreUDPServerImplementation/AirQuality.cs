﻿using MQTTnet.ManagedClient;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Text;
using System.Threading;

namespace AspNetCoreUDPServerImplementation
{
    public class AirQuality
    {
        /// <summary>
        /// Pretty print an incoming air quality measurement
        /// </summary>
        /// <param name="payload">The raw air qoulity message</param>
        public static void PrintAir(string payload)
        {
            //air,24.53,59.3996285,18.0346580,84408.00,352753093224036
            string[] parameters = payload.Split(',');
            string time = Helper.ParseTimestamp(parameters[4]);
            // depending on box version
            if (parameters.Length == 7)
            {
                Console.WriteLine(time + " >> NO2 " + parameters[1] + "ug/m3 at location: " + parameters[2] + "," + parameters[3] + " from " + parameters[5] + " with battery lvl: " + Convert.ToDouble(parameters[6]) / 1000.0 + "V");
            }
            else if (parameters.Length == 6)
            {
                Console.WriteLine(time + " >> NO2 " + parameters[1] + "ug/m3 at location: " + parameters[2] + "," + parameters[3] + " from " + parameters[5]);

            }
        }
        /// <summary>
        /// Parse the air measurement message and post it to the correct datastream
        /// </summary>
        /// <param name="msg">The raw incoming air quality measurement</param>
        /// <param name="ml">The MQTT client to use when posting</param>
        public static void HandleAir(string msg, ManagedMqttClient ml)
        {
            string[] parameters = msg.Split(',');

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;     // needed for doubles to accept dots as decimal points
            string type = parameters[0];
            double value = Convert.ToDouble(parameters[1]);
            double latitude = Convert.ToDouble(parameters[2]);
            double longitude = Convert.ToDouble(parameters[3]);
            string timestamp = Helper.ParseTimestamp(parameters[4]);
            string sender = parameters[5];
            double batLevel = 0.0;
            // depending on box version
            if (parameters.Length == 7)
            {
                 batLevel = Convert.ToDouble(parameters[6]);
            }

            // ask connection manager which topic and id to use when publishing 
            var ogcId = new OGCDataStreamId();
            JObject result = ogcId.ReceiveId(sender, "NB-IoT mobile platform", "NO2", "ug/m3", latitude, longitude);

            try
            {
                var dataStreamId = result["dataStreamId"];
                var mqttTopic = result["mqttTopic"];
                var mqttServer = result["mqttServer"];

                if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                {
                    Console.WriteLine("Did not receive a datastream ID from the remote service");
                    return;
                }

                int iotId = Convert.ToInt32(dataStreamId);

                string json = OGCDataStreamId.GetJson(iotId, timestamp, latitude, longitude, value, "NO2");

                byte[] buffer = Encoding.UTF8.GetBytes(json);
                MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
            }
            catch (Exception e)
            {
                Console.WriteLine("HandleAir error: " + e.Message);
            }
            if (batLevel != 0.0)
            {
                result = ogcId.ReceiveId(sender, "NB-IoT mobile platform ", "Battery Status", "V", latitude, longitude);

                try
                {
                    var dataStreamId = result["dataStreamId"];
                    var mqttTopic = result["mqttTopic"];
                    var mqttServer = result["mqttServer"];

                    if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                    {
                        Console.WriteLine("Did not receive a datastream ID from the remote service");
                        return;
                    }

                    int iotId = Convert.ToInt32(dataStreamId);

                    string json = OGCDataStreamId.GetJson(iotId, timestamp, latitude, longitude, batLevel/1000.0, "Battery Status");

                    byte[] buffer = Encoding.UTF8.GetBytes(json);
                    MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
                }
                catch (Exception e)
                {
                    Console.WriteLine("HandleAir error: " + e.Message);
                }
            }
        }

    }
}
