﻿using MQTTnet.ManagedClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreUDPServerImplementation
{
    public class BridgeBox
    {
        public static void PrintBridge(string msg)
        {
            // bridge,352753093224036,25.3,55.2,50,59.3996285,18.0346580
            string[] parameters = msg.Split(',');
            Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmss") + "Bridge-box; Temp: " + parameters[2] + "C, Humidity: " + parameters[4] + "%, CO2 level: " + parameters[2] + "ppm at location: " + parameters[5] + "," + parameters[6] + " from " + parameters[1]);
        }

        public static void HandleBridge(string msg, ManagedMqttClient ml)
        {
            //  bridge,352753093224036,25.3,55.2,50,59.3996285,18.0346580
            string[] parameters = msg.Split(',');
            string sender = parameters[1];
            double temp = Convert.ToDouble(parameters[2]);
            double co2 = Convert.ToDouble(parameters[3]);
            double humid = Convert.ToDouble(parameters[4]);
            double latitude = Convert.ToDouble(parameters[5]);
            double longitude = Convert.ToDouble(parameters[6]);
            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            string description = "NB-IoT Bridge Box";

            DoSendEvent(sender, description, "Temperature", "C", latitude, longitude, temp, timestamp, ml);
            DoSendEvent(sender, description, "Humidity", "%", latitude, longitude, humid, timestamp, ml);
            DoSendEvent(sender, description, "CO2", "ppm", latitude, longitude, co2, timestamp, ml);

        }

        private static void DoSendEvent(string source, string desc, string type, string unit, double lat, double lon, double val, string timestamp, ManagedMqttClient ml)
        {
            var ogcId = new OGCDataStreamId();

            JObject result = ogcId.ReceiveId(source, desc, type, unit, lat, lon);
            try
            {
                var dataStreamId = result["dataStreamId"];
                var mqttTopic = result["mqttTopic"];
                var mqttServer = result["mqttServer"];
                var externalId = result["externalId"];
                var sensorType = result["sensorType"];
                var unitOfMeasurement = result["unitOfMeasurement"];

                if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                {
                    Console.WriteLine("Did not receive a datastream ID from the remote service");
                    return;
                }

                int iotId = Convert.ToInt32(dataStreamId);

                string json = OGCDataStreamId.GetJson(iotId, timestamp, lat, lon, val, "C");

                byte[] buffer = Encoding.UTF8.GetBytes(json);
                MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
