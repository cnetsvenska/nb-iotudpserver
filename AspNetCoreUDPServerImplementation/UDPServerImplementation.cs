﻿using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.ManagedClient;
using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace AspNetCoreUDPServerImplementation
{
    class UDPServerImplementation
    {
        private readonly ILogger _logger;

        static ManagedMqttClient ml = null;
        byte[] byteData = new byte[1024];   // used to store received data
        private Socket serverSocket = null;
        private int port;
        /// <summary>
        /// Default constructor
        /// </summary>
        public UDPServerImplementation()
        {
            port = 55050;
            var logFactory = new LoggerFactory()
           .AddConsole(LogLevel.Warning)
           .AddConsole()
           .AddDebug();

            var logger = logFactory.CreateLogger<UDPServerImplementation>();
            _logger = logger;
        }

        /// <summary>
        /// Constructor with configurable port
        /// </summary>
        /// <param name="port">The port to use for incoming data</param>
        public UDPServerImplementation(int port)
        {
            this.port = port;
        }

        /// <summary>
        /// Start the UDP server, create and set up MQTT client and open socket for incoming data
        /// </summary>
        public void Start()
        {
            StartMqttClient();
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            serverSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, port));
            EndPoint newClientEP = new IPEndPoint(IPAddress.Any, 0);
            serverSocket.BeginReceiveFrom(byteData, 0, byteData.Length, SocketFlags.None, ref newClientEP, DoReceiveFrom, newClientEP);
        }

        /// <summary>
        /// The method that handles incoming data on the specified port
        /// </summary>
        /// <param name="iar">The payload and other parameters</param>
        private void DoReceiveFrom(IAsyncResult iar)
        {
            EndPoint clientEP = new IPEndPoint(IPAddress.Any, 0);
            int dataLen = 0;
            byte[] data = null;
            try
            {
                dataLen = serverSocket.EndReceiveFrom(iar, ref clientEP);
                data = new byte[dataLen];
                Array.Copy(byteData, data, dataLen);
            }
            finally
            {
                HandleMessage(Encoding.ASCII.GetString(data, 0, dataLen));

                EndPoint newClientEP = new IPEndPoint(IPAddress.Any, 0);
                serverSocket.BeginReceiveFrom(byteData, 0, byteData.Length, SocketFlags.None, ref newClientEP, DoReceiveFrom, newClientEP);
            }
        }

        /// <summary>
        /// Stop the UDP server and dispose of resourcers
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            ml.StopAsync();
            serverSocket.Close();
            serverSocket = null;
            return true;
        }

        /// <summary>
        /// Depending on message format, handle them differently
        /// </summary>
        /// <param name="message">The raw message</param>
        private void HandleMessage(string message)
        {
            Console.WriteLine(message);
            // when the device first boots a startup message is sent
            if (message.StartsWith("Starting"))
            {
                SystemInfo.PrintSystemInfo(message);
                SystemInfo.HandleSysInfo(message, ml);

                Console.WriteLine("---------------------------------");
            }
            else if (message.StartsWith("air"))
            {
                AirQuality.PrintAir(message);
                AirQuality.HandleAir(message, ml);

                Console.WriteLine("---------------------------------");
            }
            else if (message.StartsWith("PM"))
            {
                PMairQuality.PrintPM(message);
                PMairQuality.HandlePM(message, ml);

                Console.WriteLine("---------------------------------");
            }
            else if (message.StartsWith("sound"))
            {
                SoundLevel.PrintSound(message);
                SoundLevel.HandleSound(message, ml);

                Console.WriteLine("---------------------------------");
            }
            else if (message.StartsWith("heartbeat"))
            {
                Heartbeat.PrintHeartbeat(message);
                /* TODO: send heartbeat messages to it's own datastream for monotoring purposes */
                Console.WriteLine("---------------------------------");
            }
            else if (message.StartsWith("bridge"))
            {
                BridgeBox.PrintBridge(message);
                BridgeBox.HandleBridge(message, ml);
                Console.WriteLine("---------------------------------");
            }
        }
        /// <summary>
        /// Starts the MQTT client
        /// </summary>
        public static void StartMqttClient()
        {
            //configure and start a managed MQTT client
            var options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(new MqttClientOptionsBuilder()
                    .WithClientId(System.Guid.NewGuid().ToString())
                    .WithTcpServer("galileocloud.goeasyproject.eu", 1883)
                    .WithCredentials("guest", "guest")
                    .Build())
                .Build();
            ml = (ManagedMqttClient)new MqttFactory().CreateManagedMqttClient();
            ml.StartAsync(options).Wait();
            
            // add logging for mqtt
            MQTTnet.Diagnostics.MqttNetGlobalLogger.LogMessagePublished += (s, e) =>
            {
                var trace = $">> [{e.TraceMessage.Timestamp:O}] [{e.TraceMessage.ThreadId}] [{e.TraceMessage.Source}] [{e.TraceMessage.Level}]: {e.TraceMessage.Message}";
                if (e.TraceMessage.Exception != null)
                {
                    trace += Environment.NewLine + e.TraceMessage.Exception.ToString();
                }

                CultureInfo ci = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("sv-SE");

                string fileName = "MQTT" + DateTime.Now.ToShortDateString() + ".txt";

                string logFolder = ".\\";

                FileStream w = File.Open(logFolder + fileName, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.Write);
                StreamWriter sw = new StreamWriter(w, System.Text.Encoding.Default);
                sw.Write(trace + Environment.NewLine);
                sw.Close();

                Thread.CurrentThread.CurrentCulture = ci;
            };
        }        
    }
}
