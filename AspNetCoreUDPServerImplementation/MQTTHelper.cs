﻿using MQTTnet;
using MQTTnet.ManagedClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreUDPServerImplementation
{
    public class MQTTHelper
    {
        public static void SendMQTTEvent(string topic, byte[] payload, ManagedMqttClient client)
        {
            try
            {
                var message = new MqttApplicationMessageBuilder()
                    .WithTopic(topic)
                    .WithPayload(payload)
                    .WithAtLeastOnceQoS()
                    .Build();

                List<MqttApplicationMessage> mess = new List<MqttApplicationMessage>();
                mess.Add(message);
                client.PublishAsync(mess);
            }
            catch (Exception e)
            {
                Console.WriteLine("SendMQTTEvent Failed: " + e.Message + " Topic: " + topic);
            }
        }
    }
}
