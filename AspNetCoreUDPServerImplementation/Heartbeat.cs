﻿using MQTTnet.ManagedClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AspNetCoreUDPServerImplementation
{
    public class Heartbeat
    {
        /// <summary>
        /// Pretty print an incoming heartbeat update
        /// </summary>
        /// <param name="payload">The raw message</param>
        public static void PrintHeartbeat(string payload)
        {
            //heartbeat,352753093224036,3924
            string[] parameters = payload.Split(',');
            Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmss") + " HEARTBEAT received from " + parameters[1] + " with battery level: " + Convert.ToDouble(parameters[2]) / 1000.0 + "V");
        }

        /// <summary>
        /// Parse the heartbeat message and post it to the correct datastream
        /// </summary>
        /// <param name="msg">The raw message</param>
        /// <param name="ml">The MQTT client to use</param>
        public static void HandleHeartbeat(string msg, ManagedMqttClient ml)
        {
            string[] parameters = msg.Split(',');

            if (parameters.Length != 3)
            {
                return;
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;     // needed for doubles to accept dots as decimal points
            string source = parameters[1];
            double batLevel = Convert.ToDouble(parameters[2]);
            double latitude = Convert.ToDouble(parameters[3]);
            double longitude = Convert.ToDouble(parameters[4]);
            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            // ask connection manager which topic and id to use when publishing 
            var ogcId = new OGCDataStreamId();
            JObject result = ogcId.ReceiveId(source, "NB-IoT mobile platform", "Heartbeat", "V", latitude, longitude);

            try
            {
                var dataStreamId = result["dataStreamId"];
                var mqttTopic = result["mqttTopic"];
                var mqttServer = result["mqttServer"];

                if (dataStreamId == null || mqttServer == null || mqttTopic == null)
                {
                    Console.WriteLine("Did not receive a datastream ID from the remote service");
                    return;
                }

                int iotId = Convert.ToInt32(dataStreamId);

                string json = OGCDataStreamId.GetJson(iotId, timestamp, latitude, longitude, batLevel/1000.0, "Heartbeat");

                byte[] buffer = Encoding.UTF8.GetBytes(json);
                MQTTHelper.SendMQTTEvent(mqttTopic.ToString(), buffer, ml);
            }
            catch (Exception e)
            {
                Console.WriteLine("HandleHeartbeat error: " + e.Message);
            }
        }
    }
}
