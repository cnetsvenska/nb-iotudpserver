﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreUDPServerImplementation
{
    public class Helper
    {
        public static string ParseTimestamp(string timestamp)
        {
            timestamp = timestamp.PadLeft(9, '0');
            string month = DateTime.Now.Month.ToString();
            month = month.PadLeft(2, '0');
            string day = DateTime.Now.Day.ToString();
            day = day.PadLeft(2, '0');
            return DateTime.Now.Year.ToString() + month + day + timestamp.Substring(0, timestamp.Length - 3);
        }
    }
}
