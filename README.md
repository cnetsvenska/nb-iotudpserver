## UDP Server Implementation

An UDP server which listens to incoming messages sent from mobile sensing platforms (see https://bitbucket.org/cnetsvenska/mobilesensinggateway/src/master/ ) using NB-IoT to send data. Basic workflow is as follows; listen for incoming data on port 55050 and forward each incoming message to a task depening on content.
There are mainly 4 types of message types supported as of now and its easy to extend this to include more in the future. The supported messages are the following:


Startup messages  
``
starting,352753093224036,3670,5
``  

Air quality measurement  
``
air,24.53,59.3996285,18.0346580,84408.00,352753093224036
``  

Sound level measurement  
``
sound,35.4,44.6,39.3,37.7,59.3996285,18.0346580,84408.00,352753093224036
``  

Heartbeat message  
``
heartbeat,352753093224036,3670
``  

What the different parameters in the messages mean can be found in the comments of the code.

When receiving a message, the server parses it and then asks a service for a datastream ID to use for publishing the observation. If the message comes from a already known device the service just returns the ID it knows, otherwise it is created and the returned.

With the datastream ID known, the raw message is formatted to a JSON string following the OGC sensor things standard. It is then published as a MQTT payload to the topic reflected by the ID.

The UDP server also logs all incoming messages with the known formats.
## Running the UDP server

The UDP server is deployed as a docker container

To commit changes made in the code it first needs to be published on dockerhub. Right click the solution in Visual Studio and choose Publish. Choose the destination and add an image tag reflecting the current version of the project as well as the repository to publish to. When this is done, click publish.

To deploy the newly made changes, browse to the address above and find the UDP server container. If it's running press **Stop** before pressing **Duplicate/Edit**. In this menu, type the name of the newly created image with its tag included and also change the name to match the version. After this all thats left is to **Deploy the container** by pressing the button and everything should be up and running again.

## Affiliation
![GOEASY](https://bitbucket.org/cnetsvenska/nb-iotudpserver/raw/master/images/GOEASY_HD_Logo2.png)
This work is supported by the European Commission through the [GOEASY H2020 PROJECT](https://goeasyproject.eu/) under grant agreement No 776261.